# Use an OpenJDK base image
FROM openjdk:17-jdk-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the Spring PetClinic JAR file from the GitLab artifact directory into the container
COPY target/*.jar /app/spring-petclinic.jar

# Expose the port that the Spring Boot application will run on
EXPOSE 8080

# Specify the command to run your Spring Boot application when the container starts
CMD ["java", "-jar", "spring-petclinic.jar"]